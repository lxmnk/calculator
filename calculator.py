# Задание максимум (на 5): вычислить значение введённого выражения с учётом
# приоритета операций. При этом необходимо обрабатывать исключительные
# ситуации (например, деление на 0). Кроме того, нужно реализовать поддержку
# одной встроенной функции от двух переменных (например, log(a, b)). При этом
# аргументы функции сами являются  выражениями (в том числе содержат эту
# функцию). Должна быть реализована возможность сравнительно простого
# изменения встроенной функции по требованию преподавателя (например,
# заменить log(a, b) на pow(a, b)). Встроенная функция имеет наивысший
# приоритет. Скобки служат для изменения порядка действий (обычная
# математика).

# http://ru.vlab.wikia.com/wiki/Обратная_польская_запись

from operator import mul, truediv, add, sub, lshift, rshift
from collections import namedtuple
from math import exp, factorial as fact

Operator = namedtuple('Operator', 'function, priority, type')
operators = {'pow': Operator(pow, 5, 'function, binary'),
             'exp': Operator(exp, 5, 'function, unary'),
             '—': Operator(lambda x: -(x), 5, 'left-associative, unary'),
             '!': Operator(fact, 5, 'right-associative, unary'),
             '*': Operator(mul, 4, 'left-associative, binary'),
             '/': Operator(truediv, 4, 'left-associative, binary'),
             '+': Operator(add, 3, 'left-associative, binary'),
             '-': Operator(sub, 3, 'left-associative, binary'),
             '<<': Operator(lshift, 2, 'right-associative, binary'),
             '>>': Operator(rshift, 2, 'right-associative, binary'),
             '(': Operator(None, 1, 'bracket'),
             ')': Operator(None, 1, 'bracket')}


def parse_input(string):
    result = []
    string = string.replace(' ', '')
    string = string.replace('\n', '')
    number, operator = '', ''
    reading_number, reading_operator = False, False
    for x in string:
        if (x.isdigit() or x == '.') and not reading_number:
            if operator:
                result.append(operator)
            operator = ''
            number = x
            reading_number = True
            reading_operator = False
        elif (x.isdigit() or x == '.') and reading_number:
            number += x
        elif x not in '(),' and not reading_operator:
            if number:
                result.append(number)
            number = ''
            operator = x
            reading_number = False
            reading_operator = True
        elif x not in '(),' and reading_operator:
            if operator in operators:
                result.append(operator)
                operator = ''
            operator += x
        elif x in '(),':
            if number:
                result.append(number)
            if operator:
                result.append(operator)
            result.append(x)
            number, operator = '', ''
            reading_number, reading_operator = False, False
    if number:
        result.append(number)
    if operator:
        result.append(operator)
    return result


def numbers_to_float(iterable):
    result = []
    for x in iterable:
        if x.isdigit():
            result.append(int(x))
            continue
        try:
            result.append(float(x))
        except ValueError:
            result.append(x)
    return result


def pop_until_open_bracket(stack, result):
    try:
        while stack[-1] != '(':
            result.append(stack.pop())
    except IndexError:
        #print('Bracket missed!')
        exit(1)


def to_reverse_polish_notation(iterable):
    stack = []
    result = []
    for x in iterable:
        if type(x) == int or type(x) == float:
            result.append(x)
        elif x == ',':
            pop_until_open_bracket(stack, result)
        elif x == ')':
            pop_until_open_bracket(stack, result)
            stack.pop()
        elif 'function' in operators[x].type or x == '(':
            stack.append(x)
        elif 'left-associative' in operators[x].type:
            while stack and \
                    operators[x].priority <= operators[stack[-1]].priority:
                result.append(stack.pop())
            stack.append(x)
        elif 'right-associative' in operators[x].type:
            while stack and \
                    operators[x].priority < operators[stack[-1]].priority:
                result.append(stack.pop())
            stack.append(x)
        else:
            raise ValueError('Unexpected type of value!')
    while stack:
        result.append(stack.pop())
    return result


def wrong_args(args, operator):
    s = 's' if type(args) == tuple else ''
    #print('Wrong argument{s} {args} was passed to \'{operator}\''
    #      .format(s=s, args=args, operator=operator))
    exit(1)


def resolve_polish_notation(iterable):
    stack = []
    try:
        for x in iterable:
            if type(x) == int or type(x) == float:
                stack.append(x)
            elif operators[x].type == 'left-associative, binary' or \
                    operators[x].type == 'right-associative, binary' or \
                    operators[x].type == 'function, binary':
                last, prev = stack.pop(), stack.pop()
                try:
                    stack.append(operators[x].function(prev, last))
                except (ZeroDivisionError, ValueError, TypeError):
                    wrong_args((last, prev), x)
            elif operators[x].type == 'left-associative, unary' or \
                    operators[x].type == 'right-associative, unary' or \
                    operators[x].type == 'function, unary':
                last = stack.pop()
                try:
                    stack.append(operators[x].function(last))
                except (ZeroDivisionError, ValueError, TypeError):
                    wrong_args(last, x)
            else:
                raise ValueError('Unexpected type of value!')
    except IndexError:
        #print('Error in expression!')
        exit(1)
    return stack[0]


def chain(args, iterable):
    temp = iterable[0](args)
    for i in range(1, len(iterable)):
        temp = iterable[i](temp)
    return temp


def resolve(string):
    return chain(string, [parse_input,
                          numbers_to_float,
                          to_reverse_polish_notation,
                          resolve_polish_notation])


#string = '256 >> 3 / (10 - 42) * —2 * (3 + 5!) / pow(2, 3) + exp(3) * (5 << 4)'
#print(string)
#print(resolve(string))
