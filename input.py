import os
import curses
import curses.textpad
from curses import wrapper

from calculator import *

UP_MARGIN = 2
HOR_MARGIN = 2
VERT_PADDING = 1
HOR_PADDING = 2

APP_TITLE = 'space1flash\'s calculator v0.3'
RESULT_TITLE = 'Result: '

expression = ''
index = 0
terminal = None
columns, lines = None, None


def draw_frame():
    terminal.addstr(UP_MARGIN, HOR_MARGIN,
                    '─' * (columns - HOR_MARGIN * 2))
    terminal.addstr(UP_MARGIN + VERT_PADDING * 2, HOR_MARGIN,
                    '─' * (columns - HOR_MARGIN * 2))
    terminal.addstr(UP_MARGIN + VERT_PADDING, HOR_MARGIN, '│')
    terminal.addstr(UP_MARGIN + VERT_PADDING, columns - HOR_MARGIN - 1, '│')
    terminal.addstr(UP_MARGIN, HOR_MARGIN, '┌')
    terminal.addstr(UP_MARGIN, columns - HOR_MARGIN - 1, '┐')
    terminal.addstr(UP_MARGIN + VERT_PADDING * 2, HOR_MARGIN, '└')
    terminal.addstr(UP_MARGIN + VERT_PADDING * 2, columns - HOR_MARGIN - 1, '┘')


def keystroke_handler(value):
    global expression, index
    if value == 260:
        textbox.do_command(curses.KEY_LEFT)
        index -= 1
    if value == 261:
        textbox.do_command(curses.KEY_RIGHT)
        index += 1
    char = chr(value)
    if value == curses.KEY_BACKSPACE:
        if index == len(expression):
            if len(expression) > 1:
                expression = expression[:-1]
                index -= 1
            elif len(expression):
                expression = ''
                index = 0
        elif index < len(expression):
            if index == 0:
                return
            else:
                index -= 1
                expression = expression[:index] + expression[index + 1:]
        else:
            index -= 1
        textbox.do_command(8)
        print_result()
        return
    if value > 127:
        return
    if index == len(expression):
        expression += char
        index += 1
    else:
        expression = expression[:index] + char + expression[index:]
        index += 1
    print_result()
    return char


def print_result():
    try:
        result = resolve(expression)
    except:
        result = ''
    terminal.addstr(UP_MARGIN + VERT_PADDING * 2 + 1,
                    HOR_MARGIN + 1,
                    RESULT_TITLE + str(result) +
                    ' ' * (columns - len(RESULT_TITLE) - len(str(result)) - 4))
    terminal.refresh()


def main(stdscr):
    global terminal
    terminal = stdscr
    global columns, lines
    columns, lines = os.get_terminal_size()
    terminal.addstr(1, columns // 2 - len(APP_TITLE) // 2, APP_TITLE)
    draw_frame()
    print_result()
    terminal.refresh()
    window = curses.newwin(1, columns - HOR_MARGIN * 2 - HOR_PADDING * 2,
                           UP_MARGIN + VERT_PADDING, HOR_MARGIN + HOR_PADDING)
    global textbox
    textbox = curses.textpad.Textbox(window, insert_mode=True)
    try:
        textbox.edit(keystroke_handler)
    except KeyboardInterrupt:
        exit(0)


if __name__ == '__main__':
    wrapper(main)
